-This diary file is written by Julian Hansel (E44055067) for the course of Professional Skills for Engineering the Third Industrial Revolution.

## 2019-09-26

* Today's topic was fake news and how dangerous it is to the world.
* I found that serious problem for everyone that believing to fake news might affect the big chaotic.
* We can have a perspective and spread that as long as we're willing to listen to various voices.

## 2019-10-03

* Classmates sharing their diary with us.
* Change our groups for Week 4-6.
* Economy is the sum of transactions that make it up.
* US dollar is a really powerful currency and losing access to it is basically an economical death sentences.

## 2019-10-17

* The difference between facism and nationalism, for example, whether they care only themselves or everyone.
* Extreme thoughts might have a hard time dealing with their feelings in childhood

## 2019-10-24

* Today's topics about the health issues, especially the brain health.
* Exercise helps to increase supply of oxygen to the brain.

## 2019-10-31

* Today's topics about depression and suicide.
* A friend can suffer from depression and we can’t notice it.
* Don't choose the method of suicide as a way to run away from the problems you are facing.

## 2019-11-07

* Today's topics about difference about happiness and fulfilment.
* Instead we should acknowldge the fact that sometimes life is painful to be happy.
* We need is forgiveness instead of violence.

## 2019-11-14

* Different laws and legal system in Taiwan and U.K
* Not illegal to take video if we talk with police and we must to look some condition when we want to take record for example there some body suicide we can’t take some video.

## 2019-11-21

* Topic for the video that I watched about we need to remake the internet 
* The technology we wanted could destroy ourselves.
* Most of us have smartphones that can use the internet anytime and anywhere.
* Since everyone has the Internet all the time, it will give feedback according to the actions of people, and the whole human will turned into behavior modification.
* I strongly agree that there is an urgent need for reforms to these Internet problems.

## 2019-11-28

* Topic for the video that I watched about economy
* That government can't control economy and maintain it in the same time.
* It talk about the market control instead.
* But it shows no example that market control will succeed.
* Other countries were destroyed under free market but the show ignored them.

## 2019-12-05

* From the videos that i watched about environment guide our development.
* I got point from that video about human over-population and how to quickly reduce that.
* Best and fastest way to relieve the pressure on the earth's ecosystems ( include human too)is to commit to having no children or 1 child between 2 adults so that human numbers will drop, and adopt if you wish.
* Having 2 children from 2 adults just maintains the family status quo numbers; it does not help to reduce population.

## 2019-12-12
* On this week , we are discussion about planetary boundary for each group have different kind, and each group must to present about why that boundary is very important and share about the solution to solve for each boundary.
* We are also told to vote by the teacher to choose which groups according to each opinion are very important for the needs in this world by distributing papers and we write the group numbers that we want to vote for, and the teacher also tells us to vote by standing from our seats us if we evade the group that we think is best.
* Because there is not enough time, we are encouraged to watch videos at home

## 2019-12-19
* Successful and unproductive.
* Celebrating my friend birthday and i didnt go to work
* More time management 

## 2019-12-20
* Unsuccessful and unproductive.
* I got ankle foot during practice basketball for competition and i just take a rest in dormitory
* Be careful during practice

## 2019-12-21
* Succesful and productive.
* Got champion for basketball competition and wake up earlier prepare to competition.

## 2019-12-22
* Successful and productive
* Celebrating Christmas with a gift exchange and still got working

## 2019-12-23
* Unsuccessful and unproductive
* Only sleep all day in bed
* dont sleep too late until morning.

## 2019-12-24
* Successful and productive
* Celebrating christmas in the church

## 2019-12-26
* Today, have a lot of topic that each group presentation and we don't get anytime to watch lecture video.

## 2020-01-02
* We get to present our solution of problem
* Professor share why he teach for this course 
* Next week we must to prepare our video and our exam 
* I enjoyed for the class because there many solution impact and professor help who presented to find the answer for who ask in class 